package com.kamilstosik.mytaxi

import androidx.test.espresso.Espresso.onView
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.kamilstosik.mytaxi.activity.TaxiListActivity
import org.junit.Test

import org.junit.runner.RunWith
import org.junit.Rule
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.contrib.RecyclerViewActions
import com.kamilstosik.mytaxi.view.TaxiHolder


@RunWith(AndroidJUnit4::class)
class ListActivityTest  {

    @Rule @JvmField
    val activityTestRule: ActivityTestRule<TaxiListActivity> = ActivityTestRule(TaxiListActivity::class.java)

    @Test
    fun openMap() {
        onView(withId(R.id.list_vehicles)).check(matches(isDisplayed()))

        //wait for data to load from backend
        try {
            Thread.sleep(10000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        onView(withId(R.id.list_vehicles)).perform(RecyclerViewActions.actionOnItemAtPosition<TaxiHolder>(0, click()))
    }
}
