package com.kamilstosik.mytaxi

import androidx.test.espresso.Espresso.onView
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.kamilstosik.mytaxi.activity.TaxiListActivity
import org.junit.Test

import org.junit.runner.RunWith
import org.junit.Rule
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.contrib.RecyclerViewActions
import com.kamilstosik.mytaxi.activity.TaxiMapActivity
import com.kamilstosik.mytaxi.view.TaxiHolder


@RunWith(AndroidJUnit4::class)
class MapActivityTest  {

    @Rule @JvmField
    val activityTestRule: ActivityTestRule<TaxiMapActivity> = ActivityTestRule(TaxiMapActivity::class.java)

    @Test
    fun verifyMap() {
        onView(withId(R.id.map_taxi_fragment)).check(matches(isDisplayed()))
    }
}
