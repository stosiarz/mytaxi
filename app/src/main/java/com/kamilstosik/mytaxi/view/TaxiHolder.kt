package com.kamilstosik.mytaxi.view

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kamilstosik.mytaxi.R
import com.kamilstosik.mytaxi.model.Taxi

class TaxiHolder(private val view : View, private val callback : TaxiHolderListener) : RecyclerView.ViewHolder(view) {

    companion object {
        private const val CLOSE_DISTANCE_BREAK_POINT = 10000
    }

    fun bind(context : Context, vehicle : Taxi, position : Int) {
        val name = context.getString(R.string.vehicle_id_txt)+" "+vehicle.id
        view.findViewById<TextView>(R.id.list_row_name).text = name
        view.findViewById<TextView>(R.id.list_row_type).text = vehicle.fleetType
        view.findViewById<CardView>(R.id.list_row_map_btn).setOnClickListener { callback.openMap(position) }

        val distance = vehicle.distance
        val distanceView = view.findViewById<TextView>(R.id.list_row_distance)
        distanceView.text = distanceToString(distance)
        calculateDistanceColor(distanceView, distance)
    }

    /**
     * Distance color should be green for distances below 1000 meters
     */
    private fun calculateDistanceColor(distanceView : TextView, distance : Int) {
        distanceView.setTextColor(Color.RED)
        if(distance < CLOSE_DISTANCE_BREAK_POINT) {
            distanceView.setTextColor(Color.GREEN)
        }
    }

    private fun distanceToString(distance : Int) : String {
        return distance.toString()+"m"
    }
}