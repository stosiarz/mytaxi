package com.kamilstosik.mytaxi.view

interface TaxiHolderListener {
    fun openMap(position: Int)
}