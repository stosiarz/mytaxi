package com.kamilstosik.mytaxi.network

import com.kamilstosik.mytaxi.model.TaxiList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    /**
     * Example url to connect to get vehicles data
     * https://fake-poi-api.mytaxi.com/?p1Lat={Latitude1}&p1Lon={Longitude1}&p2Lat={Latitude2}&p2Lon={Longitude2}
     */
    @GET("?")
    fun vehiclesList(
        @Query("p1Lat") Latitude1: Double,
        @Query("p1Lon") Longitude1: Double,
        @Query("p2Lat") Latitude2: Double,
        @Query("p2Lon") Longitude2: Double): Observable<TaxiList>
}