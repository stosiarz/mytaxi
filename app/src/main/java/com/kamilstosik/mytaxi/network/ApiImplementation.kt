package com.kamilstosik.mytaxi.network

import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.model.Direction
import com.kamilstosik.mytaxi.model.Hamburg
import com.kamilstosik.mytaxi.model.TaxiList
import com.kamilstosik.mytaxi.repository.DirectionsRepositoryListener
import com.kamilstosik.mytaxi.repository.TaxiRepositoryListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class ApiImplementation : ApiCalls {

    @Inject
    lateinit var apiManager: ApiManager

    init {
        Injector().mainComponent?.inject(this)
    }

    /**
     * Return sorted list of vehicles once fetched from server
     * Sort the way that TAXI vehicle is always at the top of the list
     */
    override fun fetchVehicles(listener: TaxiRepositoryListener) {
        getVehicleObservable().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { result -> result.vehicles.sortedBy { it.fleetType }.reversed() as ArrayList }
            .subscribe (listener::handleResults, listener::handleError)
    }

    override fun fetchDirections(origin : String, destination : String, key : String, listener: DirectionsRepositoryListener) {
        getDirectionsObservable(origin, destination, key).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (listener::handleResults, listener::handleError)
    }

    private fun getVehicleObservable() : Observable<TaxiList> {
        return apiManager.serviceTaxi.vehiclesList(
            Hamburg.boundA.latitude, Hamburg.boundA.longitude,
            Hamburg.boundB.latitude, Hamburg.boundB.longitude)
    }

    private fun getDirectionsObservable(origin : String, destination : String, key : String) : Observable<Direction> {
        return apiManager.serviceGoogle.directions(origin, destination, key)
    }
}