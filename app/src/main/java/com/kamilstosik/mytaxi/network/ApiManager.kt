package com.kamilstosik.mytaxi.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager {

    companion object {
        private const val URL_TAXI = "https://fake-poi-api.mytaxi.com/"
        private const val URL_GOOGLE = "https://maps.googleapis.com/"
        val instance = ApiManager()
    }

    private val logging = HttpLoggingInterceptor()
    private val retrofitTaxi: Retrofit
    private val retrofitDirections: Retrofit

    val serviceTaxi : ApiService
    val serviceGoogle : ApiServiceGoogle

    init {
        retrofitTaxi = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(buildClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(URL_TAXI)
            .build()

        retrofitDirections = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(buildClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(URL_GOOGLE)
            .build()

        serviceTaxi = retrofitTaxi.create(ApiService::class.java)
        serviceGoogle = retrofitDirections.create(ApiServiceGoogle::class.java)

    }

    private fun buildClient() : OkHttpClient {
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient().newBuilder()
            .addInterceptor(logging)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}