package com.kamilstosik.mytaxi.network

import com.kamilstosik.mytaxi.model.Direction
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Connect to google api to fetch direction between two points
 */
interface ApiServiceGoogle {

    @GET("maps/api/directions/json?")
    fun directions(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("key") key: String) : Observable<Direction>
}