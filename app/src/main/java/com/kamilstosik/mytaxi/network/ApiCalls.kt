package com.kamilstosik.mytaxi.network

import com.kamilstosik.mytaxi.repository.DirectionsRepositoryListener
import com.kamilstosik.mytaxi.repository.TaxiRepositoryListener

/**
 * List all the api calls available to the application
 */
interface ApiCalls {
    fun fetchDirections(origin : String, destination : String, key :  String, listener: DirectionsRepositoryListener)
    fun fetchVehicles(listener: TaxiRepositoryListener)
}