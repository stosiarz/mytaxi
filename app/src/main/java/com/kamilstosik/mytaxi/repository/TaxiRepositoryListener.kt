package com.kamilstosik.mytaxi.repository

import com.kamilstosik.mytaxi.model.Taxi

interface TaxiRepositoryListener {
    fun handleResults(vehicles : ArrayList<Taxi>)
    fun handleError(t : Throwable)
}