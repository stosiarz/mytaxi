package com.kamilstosik.mytaxi.repository

import com.kamilstosik.mytaxi.model.Direction

interface DirectionsRepositoryListener {
    fun handleResults(directions : Direction)
    fun handleError(t : Throwable)
}