package com.kamilstosik.mytaxi.repository

import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.network.ApiImplementation
import javax.inject.Inject

open class DirectionsRepository {

    @Inject
    lateinit var api: ApiImplementation

    init {
        Injector().mainComponent?.inject(this)
    }

    open fun subscribeAndFetchDirections(origin : String, destination : String, key : String, listener : DirectionsRepositoryListener) {
        api.fetchDirections(origin, destination, key, listener)
    }
}