package com.kamilstosik.mytaxi.repository

import android.content.Context
import com.google.gson.Gson
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.network.ApiImplementation
import com.kamilstosik.mytaxi.presenter.ListRepositoryListener
import javax.inject.Inject

open class TaxiRepository {

    companion object {
        const val VEHICLES_SHARED_PREFS_KEY = "vehicles_shared"
    }

    @Inject
    lateinit var apiImplementation: ApiImplementation

    @Inject
    lateinit var context: Context

    private lateinit var presenterListener : ListRepositoryListener

    init {
        Injector().mainComponent?.inject(this)
    }

    open fun subscribeAndFetchVehiclesData(listener : ListRepositoryListener) {
        presenterListener = listener
        apiImplementation.fetchVehicles(repositoryListener)
    }

    private fun saveToPreferences(vehicles: ArrayList<Taxi>) {
        val vehiclesJson = Gson().toJson(vehicles)

        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)?.let {
            with(it.edit()) {
                putString(VEHICLES_SHARED_PREFS_KEY, vehiclesJson)
                apply()
            }
        }
    }

    private var repositoryListener = object : TaxiRepositoryListener {
        override fun handleError(t: Throwable) {
            presenterListener.handleError(t)
        }

        override fun handleResults(vehicles: ArrayList<Taxi>) {
            sortAndSaveVehicles(vehicles)
            presenterListener.handleResults(vehicles)
        }

        /**
         * Initially I decided to sort by vehicleFleet but then I added distance to the user location
         * and it makes data to present nicer when sorter by the shortest distance
         */
        private fun sortAndSaveVehicles(vehicles: ArrayList<Taxi>) {
            vehicles.forEach { it.distance = presenterListener.calculateDistance(it) }
            vehicles.sortBy { it.distance }
            saveToPreferences(vehicles)
        }
    }
}