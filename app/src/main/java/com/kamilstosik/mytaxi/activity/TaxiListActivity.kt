package com.kamilstosik.mytaxi.activity

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kamilstosik.mytaxi.R
import com.kamilstosik.mytaxi.adapter.VehicleAdapter
import com.kamilstosik.mytaxi.contract.ListContract
import com.kamilstosik.mytaxi.dagger.*
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.presenter.ListPresenter
import com.kamilstosik.mytaxi.view.TaxiHolderListener
import kotlinx.android.synthetic.main.activity_list.*
import javax.inject.Inject

class TaxiListActivity : AppCompatActivity(), ListContract.View, TaxiHolderListener {

    companion object {
        const val POSITION_EXTRA = "position_vehicle"
    }

    @Inject
    lateinit var presenter: ListPresenter

    private val adapterVehicles = VehicleAdapter(this, arrayListOf(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        Injector().mainComponent?.inject(this)
        setupList()
        presenter.attach(this)
    }

    private fun setupList() {
        list_vehicles.apply {
            layoutManager = LinearLayoutManager(this@TaxiListActivity)
            adapter = adapterVehicles
        }
    }

    override fun onResume() {
        super.onResume()
        /**
         * Each time backend is queried, it returns different data
         * I decided to reload data each time user enters list is to get fresh data
         * and "simulate" the movement of vehicles because each time we enter map
         * it will look a little bit different
         */
        presenter.loadData()
    }

    override fun bindDataToAdapter(data: ArrayList<Taxi>) {
        adapterVehicles.replaceData(data)
    }

    override fun hideProgressBar() {
        list_progress.visibility = GONE
    }

    override fun showProgressBar() {
        list_progress.visibility = VISIBLE
    }

    override fun showError(error: String) {
        Toast.makeText(this, getString(R.string.error_load_vehicles), Toast.LENGTH_SHORT).show()
    }

    override fun openMap(position: Int) {
        val intent = Intent(this, TaxiMapActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(POSITION_EXTRA, position)
        startActivity(intent)
    }
}