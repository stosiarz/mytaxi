package com.kamilstosik.mytaxi.activity

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.kamilstosik.mytaxi.R
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.gson.Gson
import com.kamilstosik.mytaxi.activity.TaxiListActivity.Companion.POSITION_EXTRA
import com.kamilstosik.mytaxi.contract.MapContract
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.presenter.MapPresenter
import com.kamilstosik.mytaxi.repository.TaxiRepository
import kotlinx.android.synthetic.main.activity_map.*
import javax.inject.Inject


class TaxiMapActivity : FragmentActivity(), MapContract.View, OnMapReadyCallback {

    @Inject
    lateinit var presenter: MapPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        Injector().mainComponent?.inject(this)
        initPresenter()
        initMap()
    }

    private fun initPresenter() {
        presenter.attach(this)
        presenter.positionSelectedVehicle = intent?.extras?.getInt(POSITION_EXTRA) ?: 0
        presenter.loadVehicleDataFromSharedPrefs()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_taxi_fragment) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        presenter.setupMap(googleMap)
        presenter.loadDirectionsData()
    }

    override fun hideProgressBar() {
        map_progress.visibility = GONE
    }

    override fun showProgressBar() {
        map_progress.visibility = VISIBLE
    }

    override fun showError(error: String) {
        Toast.makeText(this, getString(R.string.error_load_direction), Toast.LENGTH_SHORT).show()
    }

    override fun getGoogleMapKey(): String {
        return getString(R.string.google_maps_key)
    }

    override fun createVehicleMarkerBitmap() : BitmapDescriptor {
        val factory = BitmapFactory.decodeResource(resources, R.drawable.arrow)
        return BitmapDescriptorFactory.fromBitmap(factory)
    }

    override fun loadVehiclesFromSharedPrefs(): String? {
        return getSharedPreferences(packageName, Context.MODE_PRIVATE).getString(TaxiRepository.VEHICLES_SHARED_PREFS_KEY, "")
    }

    override fun parseVehicleJsonToList(json: String): List<Taxi> {
        return Gson().fromJson(json , Array<Taxi>::class.java).toList()
    }
}