package com.kamilstosik.mytaxi.contract

class BaseContract {

    interface Presenter<in T> {
        fun attach(view: T)
    }

    interface View {
        fun showProgressBar()
        fun hideProgressBar()
    }
}