package com.kamilstosik.mytaxi.contract

import com.kamilstosik.mytaxi.model.Taxi

class ListContract {

    interface View: BaseContract.View {
        fun bindDataToAdapter(data: ArrayList<Taxi>)
        fun showError(error: String)
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun loadData()
    }
}