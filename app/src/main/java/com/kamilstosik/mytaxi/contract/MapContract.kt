package com.kamilstosik.mytaxi.contract

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.kamilstosik.mytaxi.model.Taxi

class MapContract {

    interface View: BaseContract.View {
        fun getGoogleMapKey() : String
        fun showError(error: String)
        fun createVehicleMarkerBitmap() : BitmapDescriptor
        fun loadVehiclesFromSharedPrefs() : String?
        fun parseVehicleJsonToList(json : String) : List<Taxi>
    }

    interface Presenter: BaseContract.Presenter<View> {
        fun loadDirectionsData()
        fun loadVehicleDataFromSharedPrefs()
        fun setupMap(map : GoogleMap?)
    }
}