package com.kamilstosik.mytaxi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.kamilstosik.mytaxi.R
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.view.TaxiHolder
import com.kamilstosik.mytaxi.view.TaxiHolderListener

open class VehicleAdapter(private val context: Context, private val dataVehicles: ArrayList<Taxi>, private val callback: TaxiHolderListener) : Adapter<TaxiHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxiHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_row, parent, false)
        return TaxiHolder(view, callback)
    }

    override fun onBindViewHolder(holder: TaxiHolder, position: Int) {
        holder.bind(context, dataVehicles[position], position)
    }

    override fun getItemCount(): Int {
        return dataVehicles.size
    }

    open fun replaceData(data: ArrayList<Taxi>) {
        dataVehicles.clear()
        dataVehicles.addAll(data)
        notifyDataSetChanged()
    }
}