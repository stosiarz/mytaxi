package com.kamilstosik.mytaxi.presenter

import com.kamilstosik.mytaxi.model.Taxi

interface ListRepositoryListener {
    fun handleResults(vehicles : ArrayList<Taxi>)
    fun handleError(t : Throwable)
    fun calculateDistance(vehicle: Taxi) : Int
}