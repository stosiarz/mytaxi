package com.kamilstosik.mytaxi.presenter

import android.graphics.Color
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.kamilstosik.mytaxi.contract.MapContract
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.model.Direction
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.model.User
import com.kamilstosik.mytaxi.repository.DirectionsRepository
import com.kamilstosik.mytaxi.repository.DirectionsRepositoryListener
import javax.inject.Inject

class MapPresenter : MapContract.Presenter, DirectionsRepositoryListener {

    companion object {
        private const val MAP_ZOOM_DEFAULT = 10.0f
    }

    @Inject
    lateinit var directionsRepository: DirectionsRepository

    var positionSelectedVehicle = 0

    var vehicles: List<Taxi>? = null
    private lateinit var view: MapContract.View
    private var mapTaxi: GoogleMap? = null

    init {
        Injector().mainComponent?.inject(this)
    }

    override fun attach(view: MapContract.View) {
        this.view = view
    }

    override fun handleError(t: Throwable) {
        view.hideProgressBar()
        view.showError(t.message.toString())
    }

    override fun handleResults(directions: Direction) {
        view.hideProgressBar()
        drawDirectionPathOnMap(directions.routes[0].overview_polyline.points)
    }

    override fun loadDirectionsData() {
        view.showProgressBar()
        getSelectedVehicle()?.let {
            directionsRepository.subscribeAndFetchDirections(
                buildOriginForDirectionApi(),
                buildDestinationForDirectionApi(it),
                view.getGoogleMapKey(),
                this
            )
        }
    }

    private fun buildOriginForDirectionApi() : String {
        return User.location.latitude.toString() + "," + User.location.longitude.toString()
    }

    private fun buildDestinationForDirectionApi(vehicle : Taxi) : String {
        val coords = vehicle.coordinate
        val vehicleLocation = LatLng(coords.latitude, coords.longitude)
        return vehicleLocation.latitude.toString() + "," + vehicleLocation.longitude.toString()
    }

    override fun loadVehicleDataFromSharedPrefs() {
        val vehicleListJson = view.loadVehiclesFromSharedPrefs()
        vehicleListJson?.let {
            if(it.isNotEmpty()) {
                vehicles = view.parseVehicleJsonToList(it)
            }
        }
    }

    override fun setupMap(map: GoogleMap?) {
        mapTaxi = map
        mapTaxi?.let {
            placeUserMarker(it)
            placeAllVehiclesOnMap(it)
            centerMapOnUserLocation(it)
        }
    }

    private fun drawDirectionPathOnMap(path : String?) {
        if(!path.isNullOrEmpty()) {
            PolyUtil.decode(path)?.let { decodedPath ->
                val lineOptions = PolylineOptions().apply {
                    addAll(decodedPath)
                    width(8f)
                    color(Color.RED)
                }
                mapTaxi?.addPolyline(lineOptions)
            }
        }
    }

    private fun centerMapOnUserLocation(map: GoogleMap) {
        map.apply { moveCamera(CameraUpdateFactory.newLatLngZoom(User.location, MAP_ZOOM_DEFAULT)) }
    }

    private fun placeUserMarker(map: GoogleMap) {
        val markerUser = MarkerOptions().apply {
            position(User.location)
            title("Me")
            flat(true)
        }

        map.apply {
            addMarker(markerUser)
        }
    }

    private fun placeAllVehiclesOnMap(map: GoogleMap) {
        val bitmapDescriptor = view.createVehicleMarkerBitmap()

        vehicles?.forEach {
            val vehicleLocation = LatLng(it.coordinate.latitude, it.coordinate.longitude)

            val markerVehicle = MarkerOptions().apply {
                position(vehicleLocation)
                icon(bitmapDescriptor)
                rotation(it.heading.toFloat())
                title(it.fleetType)
                flat(true)
            }

            map.apply {
                addMarker(markerVehicle)
            }
        }
    }

    private fun getSelectedVehicle() : Taxi? {
        return vehicles?.get(positionSelectedVehicle)
    }
}