package com.kamilstosik.mytaxi.presenter

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.kamilstosik.mytaxi.contract.ListContract
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.model.User
import com.kamilstosik.mytaxi.repository.TaxiRepository
import javax.inject.Inject

open class ListPresenter : ListContract.Presenter, ListRepositoryListener {

    @Inject
    lateinit var taxiRepository: TaxiRepository

    private lateinit var view: ListContract.View

    init {
        Injector().mainComponent?.inject(this)
    }

    override fun attach(view: ListContract.View) {
        this.view = view
    }

    override fun loadData() {
        view.showProgressBar()
        taxiRepository.subscribeAndFetchVehiclesData(this)
    }

    override fun calculateDistance(vehicle: Taxi): Int {
        val vehiclePos = LatLng(vehicle.coordinate.latitude, vehicle.coordinate.longitude)
        return SphericalUtil.computeDistanceBetween(User.location, vehiclePos).toInt()
    }

    override fun handleError(t: Throwable) {
        view.hideProgressBar()
        view.showError(t.message.toString())
    }

    override fun handleResults(vehicles: ArrayList<Taxi>) {
        view.bindDataToAdapter(vehicles)
        view.hideProgressBar()
    }
}