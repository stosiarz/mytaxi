package com.kamilstosik.mytaxi.dagger

import com.kamilstosik.mytaxi.TaxiApplication

open class Injector {
    var mainComponent : TaxiAppComponent? = getComponentFromInstance()

    open fun getComponentFromInstance() : TaxiAppComponent? {
        return TaxiApplication.getInstance()?.component
    }
}