package com.kamilstosik.mytaxi.dagger

import com.kamilstosik.mytaxi.network.ApiImplementation
import com.kamilstosik.mytaxi.network.ApiManager
import com.kamilstosik.mytaxi.presenter.ListPresenter
import com.kamilstosik.mytaxi.presenter.MapPresenter
import com.kamilstosik.mytaxi.repository.DirectionsRepository
import com.kamilstosik.mytaxi.repository.TaxiRepository
import dagger.Module
import dagger.Provides

@Module
class TaxiModule {

    @Provides
    fun provideListPresenter(): ListPresenter {
        return ListPresenter()
    }

    @Provides
    fun provideMapPresenter(): MapPresenter {
        return MapPresenter()
    }

    @Provides
    fun provideTaxiRepository(): TaxiRepository {
        return TaxiRepository()
    }

    @Provides
    fun provideDirectionsRepository(): DirectionsRepository {
        return DirectionsRepository()
    }

    @Provides
    fun provideApiImplementation(): ApiImplementation {
        return ApiImplementation()
    }

    @Provides
    fun provideApiManager(): ApiManager {
        return ApiManager.instance
    }
}