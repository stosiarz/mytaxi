package com.kamilstosik.mytaxi.dagger

import com.kamilstosik.mytaxi.TaxiApplication
import com.kamilstosik.mytaxi.activity.TaxiListActivity
import com.kamilstosik.mytaxi.activity.TaxiMapActivity
import com.kamilstosik.mytaxi.network.ApiImplementation
import com.kamilstosik.mytaxi.view.TaxiHolder
import com.kamilstosik.mytaxi.presenter.ListPresenter
import com.kamilstosik.mytaxi.presenter.MapPresenter
import com.kamilstosik.mytaxi.repository.DirectionsRepository
import com.kamilstosik.mytaxi.repository.TaxiRepository
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, TaxiModule::class])
@Singleton
interface TaxiAppComponent {
    fun inject(app: TaxiApplication)

    fun inject(listActivity: TaxiListActivity)
    fun inject(mapActivity: TaxiMapActivity)
    fun inject(listPresenter: ListPresenter)
    fun inject(mapPresenter: MapPresenter)
    fun inject(taxiRepository: TaxiRepository)
    fun inject(directionsRepository: DirectionsRepository)
    fun inject(taxiHolder: TaxiHolder)
    fun inject(apiImplementation: ApiImplementation)

}