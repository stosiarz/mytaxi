package com.kamilstosik.mytaxi.dagger

import android.content.Context
import com.kamilstosik.mytaxi.TaxiApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: TaxiApplication) {
    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext
}