package com.kamilstosik.mytaxi

import android.app.Application
import com.kamilstosik.mytaxi.dagger.AppModule
import com.kamilstosik.mytaxi.dagger.DaggerTaxiAppComponent
import com.kamilstosik.mytaxi.dagger.TaxiAppComponent
import com.kamilstosik.mytaxi.dagger.TaxiModule

class TaxiApplication : Application() {

    companion object {
        private var INSTANCE: TaxiApplication? = null
        fun getInstance(): TaxiApplication? = INSTANCE
    }

    lateinit var component: TaxiAppComponent

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        component = DaggerTaxiAppComponent.builder().appModule(AppModule(this)).taxiModule(TaxiModule()).build()
    }
}