package com.kamilstosik.mytaxi.model

import com.google.gson.annotations.SerializedName

/**
 * Model for google Directions API
 */
data class Direction(@SerializedName("routes") val routes: ArrayList<Routes>) {

    data class Routes(@SerializedName("overview_polyline") val overview_polyline: OverviewPolyline)

    data class OverviewPolyline(@SerializedName("points") val points: String)
}