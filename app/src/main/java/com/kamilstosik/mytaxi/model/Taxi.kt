package com.kamilstosik.mytaxi.model

import com.google.gson.annotations.SerializedName

/**
 * Model for test api
 */
data class TaxiList(@SerializedName("poiList") val vehicles: ArrayList<Taxi>)

data class Taxi(@SerializedName("id")
                val id: String,
                @SerializedName("fleetType")
                val fleetType: String,
                @SerializedName("heading")
                val heading: Double,
                @SerializedName("coordinate")
                val coordinate: Coordinate,
                var distance : Int) {

    override fun toString(): String {
        return "id: "+ id +
                " fleetType: " + fleetType +
                " heading: " + heading +
                " coordinate: " + coordinate.latitude+" : "+coordinate.longitude
    }
}

data class Coordinate(@SerializedName("latitude")
                      val latitude: Double,
                      @SerializedName("longitude")
                      val longitude: Double)