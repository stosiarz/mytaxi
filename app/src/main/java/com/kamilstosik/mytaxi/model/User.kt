package com.kamilstosik.mytaxi.model

import com.google.android.gms.maps.model.LatLng

/**
 * Object represents custom user location in the city center of Hamburg
 * This location should be filled by FuseLocation manager but for the
 * purpose of the test this will be default location
 */
object User {
    val location = LatLng(53.524837, 10.014260)
}