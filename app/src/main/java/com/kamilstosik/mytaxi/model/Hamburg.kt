package com.kamilstosik.mytaxi.model

import com.google.android.gms.maps.model.LatLng

/**
 * City of Hamburg bounds used to retrieve data from the server
 */
object Hamburg {
    val boundA = LatLng(53.694865, 9.757589)
    val boundB = LatLng(53.394655, 10.099891)
}