package com.kamilstosik.mytaxi

import com.kamilstosik.mytaxi.contract.ListContract
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.dagger.TaxiAppComponent
import com.kamilstosik.mytaxi.model.Coordinate
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.presenter.ListPresenter
import com.kamilstosik.mytaxi.repository.TaxiRepository
import org.junit.Assert
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ListPresenterTest  {

    val presenter = ListPresenter()
    val viewMock = mock(ListContract.View::class.java)
    val taxiRepositoryMock = mock(TaxiRepository::class.java)

    @Before
    fun setup() {
        val component = mock(TaxiAppComponent::class.java)
        val injector = mock(Injector::class.java)
        `when`(injector.getComponentFromInstance()).thenReturn(component)
        presenter.attach(viewMock)
        presenter.taxiRepository = taxiRepositoryMock
    }

    @Test
    fun loadDataTest() {
        presenter.loadData()
        verify(viewMock, times(1)).showProgressBar()
        verify(taxiRepositoryMock, times(1)).subscribeAndFetchVehiclesData(presenter)
    }

    @Test
    fun calculateDistanceTest() {
        val coord = Coordinate(10.000, 10.000)
        val taxi = Taxi("1", "TAXI", 30.000, coord, 1)
        val distance = presenter.calculateDistance(taxi)
        Assert.assertNotNull(distance)
        Assert.assertEquals(4839748, distance)
    }

    @Test
    fun handleErrorTest() {
        val error = Throwable("test error")
        presenter.handleError(error)
        verify(viewMock, times(1)).hideProgressBar()
        verify(viewMock, times(1)).showError("test error")
    }

    @Test
    fun handleResultsTest() {
        presenter.handleResults(arrayListOf())
        verify(viewMock, times(1)).bindDataToAdapter(arrayListOf())
        verify(viewMock, times(1)).hideProgressBar()
    }
}
