package com.kamilstosik.mytaxi

import com.kamilstosik.mytaxi.contract.MapContract
import com.kamilstosik.mytaxi.dagger.Injector
import com.kamilstosik.mytaxi.dagger.TaxiAppComponent
import com.kamilstosik.mytaxi.model.Coordinate
import com.kamilstosik.mytaxi.model.Direction
import com.kamilstosik.mytaxi.model.Taxi
import com.kamilstosik.mytaxi.model.User
import com.kamilstosik.mytaxi.presenter.MapPresenter
import com.kamilstosik.mytaxi.repository.DirectionsRepository
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MapPresenterTest  {

    val presenter = MapPresenter()
    val viewMock = mock(MapContract.View::class.java)
    val directionsRepositoryMock = mock(DirectionsRepository::class.java)

    @Before
    fun setup() {
        val component = mock(TaxiAppComponent::class.java)
        val injector = mock(Injector::class.java)
        `when`(injector.getComponentFromInstance()).thenReturn(component)
        presenter.attach(viewMock)
        presenter.directionsRepository = directionsRepositoryMock
    }

    @Test
    fun loadDirectionsDataTest() {
        val coord = Coordinate(10.000, 10.000)
        val taxi = Taxi("1", "TAXI", 30.000, coord, 1)
        presenter.vehicles = arrayListOf(taxi).toList()
        val origin = User.location.latitude.toString() + "," + User.location.longitude.toString()
        val destination = coord.latitude.toString() + "," + coord.longitude.toString()
        val key = "key"

        `when`(viewMock.getGoogleMapKey()).thenReturn(key)

        presenter.loadDirectionsData()
        verify(viewMock, times(1)).getGoogleMapKey()
        verify(viewMock, times(1)).showProgressBar()
        verify(directionsRepositoryMock, times(1)).subscribeAndFetchDirections(origin, destination, key, presenter)
    }


    @Test
    fun loadVehicleDataFromSharedPrefs() {
        val json = "test"
        `when`(viewMock.loadVehiclesFromSharedPrefs()).thenReturn(json)
        presenter.loadVehicleDataFromSharedPrefs()
        verify(viewMock, times(1)).loadVehiclesFromSharedPrefs()
        verify(viewMock, times(1)).parseVehicleJsonToList(json)
    }

    @Test
    fun handleError() {
        val error = Throwable("test error")
        presenter.handleError(error)
        verify(viewMock, times(1)).hideProgressBar()
        verify(viewMock, times(1)).showError("test error")
    }

    @Test
    fun handleResults() {
        val overviewPolyline = Direction.OverviewPolyline("ie|eIme~{@k@lAKTCFq@`BYr@M\\g@jAQb@{ErKQ^Sj@Y|@IZK`@")
        val route = Direction.Routes(overviewPolyline)
        val direction = Direction(arrayListOf(route))
        presenter.handleResults(direction)
        verify(viewMock, times(1)).hideProgressBar()
    }

    @Test
    fun handleResultsEmpty() {
        val overviewPolyline = Direction.OverviewPolyline("")
        val route = Direction.Routes(overviewPolyline)
        val direction = Direction(arrayListOf(route))
        presenter.handleResults(direction)
        verify(viewMock, times(1)).hideProgressBar()
    }
}
